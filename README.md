**《练习作品：单机版五子棋》** 

 *  实践项目 --- 《单机版五子棋游戏》
 *  程序实现：
 *  	（1）设计适合的窗体，并加载棋盘图片到窗体中
 *		（2）黑白棋子交互绘制
 *		（3）点击棋子能够定性地绘制在棋盘的两线交点处
 *		（4）判断胜负
 *  待实现功能：
 *		（1）悔棋
 *		（2）重置
 *		（3）自动下棋	
 *  完成时间：2012，4，21。
 *  第一次更新V1.0：2012，4，22。（注释，更改变量名,判断胜负） 	
 *  第二次更新V1.1：2012， 4，23。（注册按钮重置）

这个程序是用JAVA开发的，所以你操作系统需要安装好JAVA环境，

如果没有安装JAVA环境则请下载[jre8_86.zip](http://pan.baidu.com/s/1eSLs1Ku)，解压安装jar8_86.exe，

按指示安装完成后，双击运行：[gobang.jar](http://git.oschina.net/zirylee/gobang/attach_files)

![输入图片说明](https://git.oschina.net/zirylee/gobang/raw/master/show.png?dir=0&filepath=show.png&oid=ace2788218d411a3e7d7aeecdb213ca17a2acfdb&sha=b59b405a09551c196f3cb5e621d861196f7d66e7 "在这里输入图片标题")